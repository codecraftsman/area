package cn.wdcode.area.mapper;

import cn.wdcode.area.entity.Area;
import com.baomidou.mybatisplus.mapper.AutoMapper;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;

/**
 *
 * Area 表数据库控制层接口
 *
 */
@Mapper
public interface AreaMapper extends AutoMapper<Area> {

//    Area selectByParentId(Serializable id);

}