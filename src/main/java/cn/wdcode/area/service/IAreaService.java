package cn.wdcode.area.service;

import cn.wdcode.area.entity.Area;
import com.baomidou.framework.service.ISuperService;

import java.io.Serializable;

/**
 *
 * Area 表数据服务层接口
 *
 */
public interface IAreaService extends ISuperService<Area> {

//    Area selectByParentId(Serializable id);
}